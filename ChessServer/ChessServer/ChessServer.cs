﻿using System;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
namespace Server
{
    
    class ChessServer
    {
        static ClientRoom[] clientRooms = new ClientRoom[1025];
        public static void Main()
        {
           
            TcpListener server = null;
            try
            {
                // Set the TcpListener on port 13000.
                Int32 port = 1991;
                IPAddress localAddr = IPAddress.Any;
                int clientCount = 0;

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(localAddr, port);

                // Start listening for client requests.
                server.Start();


                // Enter the listening loop.
                while (true)
                {
                        Console.Write("Waiting for a connection... ");

                        // Perform a blocking call to accept requests.
                        // You could also user server.AcceptSocket() here.
                        TcpClient client = server.AcceptTcpClient();
                        Console.WriteLine("Connected!");
                        clientRooms[clientCount] = new ClientRoom();
                        clientRooms[clientCount].StartServer(client, clientCount);
                        clientCount++;
                        Console.WriteLine(clientCount);
                        if (clientCount == 1023)
                            clientCount = 0;
                }
            }
            catch (SocketException e)
            {
                //Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }


            Console.WriteLine("\nHit enter to continue...");
            Console.Read();
        }
        public static ClientRoom GetClientRoom(int i)
        {
            return clientRooms[i];
        }
    }

    public class ClientRoom
    {
        TcpClient clientSocket;
        NetworkStream stream;
        int ClientNumber;
        
        public void StartServer(TcpClient socket, int ClientNumber)
        {
            Console.WriteLine(ClientNumber);
            this.clientSocket = socket;
            this.ClientNumber = ClientNumber;
            Thread serverThread = new Thread(ServerChat);
            serverThread.Start();
        }

        public void MovePiece(String toSend)
        {
            byte[] msg;
            msg = System.Text.Encoding.ASCII.GetBytes(toSend);
            stream.Write(msg, 0, msg.Length);
            Console.WriteLine("Sent: {0}", toSend);
        }

        private void ServerChat()
        {
            
            // Buffer for reading data
            Byte[] bytes = new Byte[256];
            String data = null;

            stream = clientSocket.GetStream();
            

                // Get a stream object for reading and writing
                try
                {


                    int i;
                    int breaki = 0;

                    // Loop to receive all the data sent by the client.
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Console.WriteLine("Received: {0}", data);

                        // Process the data sent by the client.
                        data = data.ToUpper();
                        byte[] msg;
                        if (data == "TEST")
                        {
                            if ((ClientNumber%2) == 0)
                            {
                                msg = System.Text.Encoding.ASCII.GetBytes("Camera0");
                                Console.WriteLine("Camera 0", msg);
                            }
                                
                            else if ((ClientNumber%2) == 1)
                            {
                                msg = System.Text.Encoding.ASCII.GetBytes("Camera1");
                                Console.WriteLine("Camera 1", msg);
                            }
                            else
                            {
                                msg = System.Text.Encoding.ASCII.GetBytes("CameraX");
                            }
                                
                                
                            stream.Write(msg, 0, msg.Length);
                            Console.WriteLine("SentStream");
                            
                        }
                        else if (data == "EXIT"  || data.Length > 20)
                        {
                            breaki = 1;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("MovePiece");
                            if ((ClientNumber%2) == 0)
                                ChessServer.GetClientRoom(ClientNumber + 1).MovePiece(data);
                            if ((ClientNumber%2) == 1)
                                ChessServer.GetClientRoom(ClientNumber - 1).MovePiece(data);
                        }

                        //System.Threading.Thread.Sleep(5000);
                        // Send back a response.


                    }
                    //if (breaki == 1)
                    //{
                    //    break;
                    //}
                    // Shutdown and end connection
                    Thread.Sleep(1000);
                    Console.WriteLine("Sleep");
                }

                catch 
                {
                    Console.WriteLine("catch");
                }
            
            clientSocket.Close();

        }

        

    }
}
