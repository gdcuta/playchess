﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Net;

public class NextRoom : MonoBehaviour 
{
    public void ChessMenu()
    {
        SceneManager.LoadScene("ChessMenu");
    }
    public void Chess()
    {
        SceneManager.LoadScene("ChessGame");
    }

}
