﻿/* Main class for controlling Bishops In the Scene View
 * 
 * @author                  Peter Sassaman
 * @last person to update   Peter Sassaman
 * @version                 0.0.0         
 * @inherits                Piece
 * 
 */
public class Bishop : Piece
{
    /*  
     *  
     * Controls what hapens when Bishop is clicked in the scene.
     * @param   void
     * @return  void
     * 
     */
    void OnMouseDown()
    {
        //  if you are the correct player and it is your turn you may click on a color of your piece if it isn't already clicked. and it will generate position cubes for the user
        //  to select where the Bishop will move.
        if (((Co.Player && GameManagement.GetTurn() && color == "BLACK") || (!Co.Player && !GameManagement.GetTurn() && color == "WHITE")) && !GameManagement.GetAlreadyClicked())
        {
            //  the bishop can move in diagonal directions at a radius of 8 with no special movement parameters(0)
            PlacePositionCubesInRow("NORTHEAST",8,0);
            PlacePositionCubesInRow("NORTHWEST",8,0);
            PlacePositionCubesInRow("SOUTHEAST",8,0);
            PlacePositionCubesInRow("SOUTHWEST",8,0);
        }
    }
}
