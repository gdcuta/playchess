﻿using System;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/* Manages comunication with the server.  Sends and receives messages.
 * 
 * @author                  Peter Sassaman
 * @last person to update   Peter Sassaman
 * @version                 0.0.0         
 * 
 */

public class Client : MonoBehaviour 
{
    //Class Variables
    private static String message;

    //Instance Variables
    private GameObject GameManager;
    public GameObject Camera0;
    public GameObject Camera1;
    private bool Cam0;
    private bool Cam1;
    private bool oneSkip;
    private bool leaveRoom;
    //leave out of start because have to put array in at declaration in this case
    private int[] responseData3 = {-1, -1, -1};
    private bool isAlive;
    private Thread clientThread;
    private NetworkStream stream;
    /*
     * 
     * Initialize Client variables and start client thread
     * @Param none
     * @Retun none
     * 
     */
    void Start()
    {
        message = "TEST";
        GameManager = GameObject.Find("GameManager");
        Cam0 = false;
        Cam1 = false;
        oneSkip = false;
        leaveRoom = false;
        isAlive = true;
        clientThread = new Thread(Connect);
        clientThread.IsBackground = true;
        clientThread.Start();
    }
    /*
     * 
     * processes the main thread for the client.
     * -leave room when needed
     * -activates the correct camera sets the player.
     * -processes the piece movements based on the messages in responseData3
     * @param none
     * @return none
     * 
     */
    void Update()
    {
        if (leaveRoom == true)
            SceneManager.LoadScene("ChessMenu");
        if (Cam0)
        {
            Camera0.SetActive(true);
        }  
        if (Cam1)
        {
            Camera1.SetActive(true);
            Co.Player = true;
        } 
        //if there is data to process then process piece movements
        if(responseData3[0] != -1 && responseData3[1] != -1 && responseData3[2] != -1)
        {
            //move piece using the data from the message
            Piece tempPiece = GameManager.GetComponent<GameManagement>().GetPiece(responseData3[0]).GetComponent<Piece>();
            tempPiece.MovePiece(responseData3[1], responseData3[2]);

            //if it is the kings first turn and the king is trying to move 2 spaces and the rook it wants to castle with is also on its first turn then we process the castle
            if (GameManager.GetComponent<GameManagement>().GetPiece(responseData3[0]).name.Contains("King"))
            {
                if (tempPiece.firstTurn && responseData3[2] == 2)
                {
                    Piece tempRook = GameObject.Find("GameManager").GetComponent<GameManagement>().GetAllPieces(tempPiece.GridX, 0).GetComponent<Piece>();
                    if (tempRook.firstTurn)
                        tempRook.MovePiece2(tempPiece.GridX, tempPiece.GridY + 1);
                    tempRook.firstTurn = false;
                }
                else if (tempPiece.firstTurn && responseData3[2] == 6)
                {
                    Piece tempRook = GameObject.Find("GameManager").GetComponent<GameManagement>().GetAllPieces(tempPiece.GridX, 7).GetComponent<Piece>();
                    if (tempRook.firstTurn)
                        tempRook.MovePiece2(tempPiece.GridX, tempPiece.GridY - 1);
                    tempRook.firstTurn = false;
                }
            }
            //resets message back to false for next update and sets the piece we have been handling so it is not it's first turn
            tempPiece.firstTurn = false;
            responseData3[0] = -1;
            responseData3[1] = -1;
            responseData3[2] = -1;
        }
        
    }
    /*
     * 
     * used to launch a client thread to send and receive messages to and from the server stores the received messages in Responsedata3 to be used by the main thread for moving pieces
     * and gets messages to send to the server from message.
     * @param none
     * @return none
     * 
     */
    private void Connect()
    {

        //get IP to connect to from IPAdd class this is user defined
        String server = IPAdd.GetIP();

        try
        {
            // Create a TcpClient.
            // Note, for this client to work you need to have a TcpServer 
            // connected to the same address as specified by the server, port
            // combination.
            Int32 port = 1991;
            TcpClient client = new TcpClient(server, port);
            
            // Translate the passed message into ASCII and store it as a Byte array.
            Byte[] data;
            //NetworkStream stream;
            // Get a client stream for reading and writing.
            stream = client.GetStream();
            while(isAlive)
            {
                if(!oneSkip)
                {
                    data = System.Text.Encoding.ASCII.GetBytes(message);
                    // Send the message to the connected TcpServer. 
                    stream.Write(data, 0, data.Length);
                    
                }
                

                // Receive the TcpServer.response.

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                if (responseData == "Camera0")
                {
                    Cam0 = true;
                }
                else if (responseData == "Camera1")
                {
                    Cam1 = true;
                    oneSkip = true;
                }
                else if (responseData == "close")
                {
                    break;
                }
                //receive data from server about piece and movement
                else
                {
                    String[] responseData2 = responseData.Split(',');
                    responseData3 = Array.ConvertAll<string, int>(responseData2, int.Parse);
                    oneSkip = false;
                }
                //set message to empty and wait for message
                if(!oneSkip)
                {
                    message = "";
                    while (message == "")
                    {
                        Thread.Sleep(100);
                    }
                }   
            }
        //close stream and client
        stream.Close();
        client.Close();
        }
        //catch exceptions pertaining to server not existing
        catch (ArgumentNullException e)
        {
            string tempString3 = "ArgumentNullException: " + e;
            Console.WriteLine(tempString3);
            leaveRoom = true;
        }
        catch (SocketException e)
        {
            string tempString4 = "SocketException: " + e;
            Console.WriteLine(tempString4);
            leaveRoom = true;
        }

        Console.WriteLine("\n Press Enter to continue...");
        Console.Read();
    }
    /*
     * 
     * called to set the message for sending to the server
     * @param String imessage
     * @return none
     * 
     */
    public static void SetMessage(String imessage)
    {
        message = imessage;
    }
    /*
     * 
     * if someone quits the game by pushin X in the application then this will properly close the stream and the client thread so as to free up the port in the OS
     * @param none
     * @return none
     * 
     */
    void OnApplicationQuit()
    {
        stream.Close();
        clientThread.Abort();
        clientThread.Join();
        Application.Quit();
    }
    /*
     * 
     * called when the player pushes the quit button in the game.  this properly closes the stream and the client thread and returns to the chess menu
     * @param none
     * @return none
     * 
     */
    public void QuitChess()
    {
        stream.Close();
        clientThread.Abort();
        clientThread.Join();
        SceneManager.LoadScene("ChessMenu");
    }
}



