﻿using UnityEngine;
using System.Collections;

public class Pawn : Piece
{
    void OnMouseDown()
    {
        //Debug.Log("ClickyClick");
        //Debug.Log(Co.Player.ToString());
        //Debug.Log(GameManagement.GetTurn().ToString());
        //Debug.Log(color.ToString());
        //  make position cubes
        if (((Co.Player && GameManagement.GetTurn() && color == "BLACK") || (!Co.Player && !GameManagement.GetTurn() && color == "WHITE")) && !GameManagement.GetAlreadyClicked())
        {
            PositionCubeItterator = 0;
            //Debug.Log("ClickyClick");
            //PlacePositionCubesInRow("NORTHEAST",2);
            //PlacePositionCubesInRow("NORTHWEST",2);
            //PlacePositionCubesInRow("SOUTHEAST",2);
            //PlacePositionCubesInRow("SOUTHWEST",2);
            if (firstTurn)
            {
                if (color == "WHITE")
                    PlacePositionCubesInRow("SOUTH", 3, 3);
                if (color == "BLACK")
                    PlacePositionCubesInRow("NORTH", 3, 3);
            }
            else
            {
                if (color == "WHITE")
                    PlacePositionCubesInRow("SOUTH", 2, 3);
                if (color == "BLACK")
                    PlacePositionCubesInRow("NORTH", 2, 3);
            }
            if (color == "WHITE")
            {
                PlacePositionCubesInRow("SOUTHEAST", 2,1);
                PlacePositionCubesInRow("SOUTHWEST", 2,1);
            }
                
            if (color == "BLACK")
            {
                PlacePositionCubesInRow("NORTHEAST", 2,1);
                PlacePositionCubesInRow("NORTHWEST", 2,1);
            }
            
                
        }
    }
}
