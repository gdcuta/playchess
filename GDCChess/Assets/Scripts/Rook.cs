﻿using UnityEngine;
using System.Collections;

public class Rook : Piece
{
    void OnMouseDown()
    {
        //  make position cubes
        if (((Co.Player && GameManagement.GetTurn() && color == "BLACK") || (!Co.Player && !GameManagement.GetTurn() && color == "WHITE")) && !GameManagement.GetAlreadyClicked())
        {
            PlacePositionCubesInRow("NORTH",8,0);
            PlacePositionCubesInRow("SOUTH",8,0);
            PlacePositionCubesInRow("EAST",8,0);
            PlacePositionCubesInRow("WEST",8,0);
        }
    }
}
